# uefi-secureboot-ubuntu-usb

Note for myself

## Task

Have a bootable USB HDD/SSD with Linux (Ubuntu as example) which:

- is a "full install OS" with persistent data, not a live USB
- works on PC's with **UEFI** boot
- works on PC's with **Secure Boot** enabled
- has encrypted **/** and (optionally) **/boot** partitions

## Prerequisites

1. Download distro iso (i.e. Ubuntu)
1. Get USB storage device of suitable capacity
1. On installation host PC - enable UEFI and SecureBoot (if not possible to boot host OS to SecureBoot, prepare another USB with Ubuntu LiveCD, see Gotchas section :D)
1. On installation host PC - install VirtualBox or QEMU/KVM (Virtual Machine Manager)

## Process

1. Create VM machine with UEFI support (consult Sources 1)
    - QEMU/KVM machine chipset Q35, firmware UEFI (consult Sources 3)
    - VirtualBox machine chipset ICH9, EFI enabled
1. Attach USB physical drive directly to VM 
    - QEMU/KVM (consult Sources 1)
    - VirtualBox (consult Sources 4 and 5)
1. Attach downloaded distro iso as CD to VM
1. Start and boot VM from CD (so called LiveCD)
    - Check that Secure Boot enabled in VM (consult Sources 6)
1. While still in LiveCD, partition USB drive as required and install distro with encryption (consult Sources 1 and 7)
    - Do not reboot after instalaltion
1. While still in LiveCD, install GRUB with image usable with EFI Secure Boot (consult Sources 1 and 2; Ubuntu LiveCD example)

    ```bash
    sudo mkdir -p /mnt/root/boot
    sudo mount /dev/<your_root_partition> /mnt/root         # skip if you have separate /boot partition
    sudo mount /dev/<your_boot_partition> /mnt/root/boot    # skip if you don't have separate /boot partition
    sudo mount /dev/<your_efi_partition> /mnt/root/boot/efi # mount esp
    sudo apt install grub-efi-amd64-signed shim-signed      # mandatory step!
    sudo grub-install --efi-directory=/mnt/root/boot/efi --boot-directory=/mnt/root/boot --target x86_64-efi --uefi-secure-boot --removable /dev/<your_usb_device>
    ```
    or

    ```bash
    sudo mkdir -p /mnt/root/boot
    sudo mount /dev/<your_root_partition> /mnt/root         
    sudo mount /dev/<your_boot_partition> /mnt/root/boot    # skip if you don't have separate /boot partition
    sudo mount /dev/<your_efi_partition> /mnt/root/boot/efi # mount esp
    for i in /dev /dev/pts /proc /sys; do sudo mount -B $i /mnt/root$i; done
    sudo cp --remove-destination /etc/resolv.conf /mnt/root/etc
    modprobe efivars
    sudo chroot /mnt/root
    apt install grub-efi-amd64-signed shim-signed           # mandatory step!
    grub-install --target x86_64-efi --uefi-secure-boot --removable /dev/sda
    exit
    for i in /sys /proc /dev/pts /dev; do sudo umount /mnt/root$i; done
    sudo umount /mnt/boot/efi 
    sudo umount /mnt
    ```

## Gotchas

1. To use "grub-efi-amd64-signed shim-signed" it may be required to boot in Secure Boot (you can boot from Ubuntu Live USB, then chroot to your destination USB OS)
1. If chrooting it is required to "modprobe efivars" before "chroot /mnt"
1. If chrooting at some point "sudo cp /etc/resolv.conf /mnt/etc/" may require "--remove-destination"
    > remove each existing destination file before attempting to open it (contrast with --force)

## Sources

1. [Encrypted 20.04 Full Install USB that boots UEFI and BIOS on QEMU/KVM](https://askubuntu.com/a/1331236)
1. [GRUB for Secure Boot](https://forums.linuxmint.com/viewtopic.php?p=1804247&sid=dd4f09e4ef18ac5f30b426e35c8399c0#p1804247)
1. [QEMU/KVM - Secure Boot](https://wiki.debian.org/SecureBoot/VirtualMachine#virt-manager)
1. [VirtualBox - 9.7.1.1. Access to Entire Physical Hard Disk](https://www.virtualbox.org/manual/ch09.html)
1. [VirtualBox - BLKCACHE_IOERR when using real disk as vmdk](https://forums.virtualbox.org/viewtopic.php?t=86736)
1. [How to check Secure Boot enabled in Linux](https://techoverflow.net/2019/05/23/how-to-check-if-secure-boot-is-enabled-on-ubuntu/)
1. [Ubuntu Desktop 20.04 with btrfs-luks full disk encryption](https://reckoning.dev/blog/ubuntu-btrfs-guide/)
